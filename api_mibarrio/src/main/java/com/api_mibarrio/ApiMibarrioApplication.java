package com.api_mibarrio;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiMibarrioApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApiMibarrioApplication.class, args);
	}

}
