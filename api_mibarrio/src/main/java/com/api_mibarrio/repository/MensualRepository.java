package com.api_mibarrio.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.api_mibarrio.model.Mensual;

public interface MensualRepository extends JpaRepository<Mensual, Integer>{

}
