package com.api_mibarrio.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.api_mibarrio.model.Producto;

public interface ProductoRepository extends JpaRepository<Producto, Integer>{

}
