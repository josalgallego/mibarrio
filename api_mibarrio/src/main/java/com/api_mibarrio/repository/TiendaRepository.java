package com.api_mibarrio.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.api_mibarrio.model.Tienda;

public interface TiendaRepository extends JpaRepository<Tienda, Integer>{

}
