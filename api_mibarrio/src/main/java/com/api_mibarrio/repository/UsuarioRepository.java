package com.api_mibarrio.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.api_mibarrio.model.Usuario;

public interface UsuarioRepository extends JpaRepository<Usuario, Integer> {

}
